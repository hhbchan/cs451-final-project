from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
from html.parser import HTMLParser
import os
import shutil
import re

import finance_constants

input_dir = 'data/'
output_dir = 'data-finance-filtered/'

def english_tweets_filter(tweet):  
  if tweet['created_at'] is None: return False
  if tweet['lang'] != 'en':       return False
  return True

def sensitive_content_filter(tweet):
  if tweet['possibly_sensitive']: return False
  if tweet['retweeted_status'] and tweet['retweeted_status']['possibly_sensitive']: return False
  return True

common_words_pattern = re.compile('|'.join(finance_constants.common_words), flags=re.I)
companies_pattern = re.compile('|'.join(finance_constants.companies), flags=re.I)
tickers_pattern = re.compile('\\b'+'\\b|\\b'.join(finance_constants.tickers)+'\\b')
def financial_related_filter(tweet):
  text = tweet[1]
  if bool(common_words_pattern.search(text)): return True
  if bool(companies_pattern.search(text)):    return True
  if bool(tickers_pattern.search(text)):      return True
  return False

def find_full_text(tweet):
  if tweet['retweeted_status']:
    if tweet['retweeted_status']['extended_tweet']:
      return tweet['retweeted_status']['extended_tweet']['full_text']
    else:
      return tweet['retweeted_status']['text']
  elif tweet['quoted_status']:
    return tweet['text'] + ' ' + tweet['quoted_status']['text']
  elif tweet['extended_tweet']:
    return tweet['extended_tweet']['full_text']
  return tweet['text']

def find_stat_cnt(tweet, stat_name):
  count = int(tweet[stat_name])
  if tweet['retweeted_status'] and tweet['retweeted_status'][stat_name]:
    count += int(tweet['retweeted_status'][stat_name])
  if tweet['quoted_status'] and tweet['quoted_status'][stat_name]:
    count += int(tweet['quoted_status'][stat_name])
  return count

def interested_attributes_mapper(tweet):
  timestamp = int(tweet['timestamp_ms'])
  text = find_full_text(tweet)
  quote_cnt = find_stat_cnt(tweet, 'quote_count')
  reply_cnt = find_stat_cnt(tweet, 'reply_count')
  retweet_cnt = find_stat_cnt(tweet, 'retweet_count')
  favorite_cnt = find_stat_cnt(tweet, 'favorite_count')
  return (timestamp, text, quote_cnt, reply_cnt, retweet_cnt, favorite_cnt)

# Remove previous output
if os.path.isdir(output_dir):
  shutil.rmtree(output_dir)

spark = SparkSession \
    .builder \
    .appName('Filter for finance-related tweets') \
    .getOrCreate()

for day in range(1, 32):
  folder = f'2020-10-{day:02}'
  spark.read.json(f'{input_dir}{folder}/*').rdd \
    .filter(english_tweets_filter)      \
    .filter(sensitive_content_filter)   \
    .map(interested_attributes_mapper)  \
    .filter(financial_related_filter)   \
    .saveAsTextFile(output_dir + folder)
