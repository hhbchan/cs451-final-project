from pyspark import SparkConf, SparkContext
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression, LinearRegressionModel
from pyspark.sql import SQLContext
from pyspark.sql import HiveContext
from pyspark.sql.types import IntegerType
from pyspark.sql.window import Window
from pyspark.sql.functions import *
from dateutil.parser import parse
from datetime import timedelta
import argparse
import os
from datetime import datetime
from pyspark.sql.functions import unix_timestamp, from_unixtime
from pyspark.sql.functions import udf
from pyspark.sql.functions import percent_rank

stats = ["pos_sentiment", "neg_sentiment", "quote_count", "reply_count", "retweet_count", "favorite_count", "volume"]
sliding_window_size = 3

def parse_args():
  parser = argparse.ArgumentParser(description="Train and Eval Pricing Model")
  parser.add_argument("-so", "--sentiment-only", action='store_true', default=False, help="train comparison model with sentiment-only (ie positive > negative)")
  parser.add_argument("-lr", "--linear-regression", action='store_true', default=False, help="train linear regression model with sentiment and tweet metrics")
  parser.add_argument("-lrp", "--linear-regression-with-price", action='store_true', default=False, help="train linear regression model with sentiment, tweet metrics, and previous stock price")
  parser.add_argument("-f", "--force", action='store_true', default=False, help="force model to re-train")
  return parser.parse_args()

def get_tweet_dataframe(sc, input_file):
  def parse_line(line):
    arr = line.strip('()').split(',')
    return tuple(map(int, arr))

  rdd = sc.textFile(input_file).map(parse_line)
  df = rdd.toDF(["timestamp", *stats])
  
  # Create the sliding window by comparing the a row with the next x rows
  window_spec = Window.rowsBetween(Window.currentRow, sliding_window_size-1).orderBy(desc("timestamp"))
  for stat_name in stats:
    df = df.withColumn(f"sum_{stat_name}", sum(stat_name).over(window_spec).cast(IntegerType()))
    df = df.drop(stat_name)

  return df

def get_stock_dataframe(sc, input_file):
  def parse_line(line):
    arr = line.strip('()').split(',')
    # subtract 30mins to align with hourly mark
    stock_time = parse(arr[0]) - timedelta(minutes=30)
    timestamp = int(stock_time.timestamp() * 1000)
    opening = float(arr[1])
    return (timestamp, opening)

  rdd = sc.textFile(input_file).map(parse_line)
  df = rdd.toDF(["timestamp", "opening"])

  # Add third column for the opening price 3 hours prior (which would be the row 3 below)
  # Note that the last 3 rows contains null and are filtered away
  window_spec = Window.orderBy(desc("timestamp"))
  lead_col = lead("opening", sliding_window_size).over(window_spec)
  df = df.withColumn("p_opening", lead_col)
  df = df.filter(df.p_opening.isNotNull())

  return df

def create_evaluation_fn(prediction_fn, tp, fn, fp, tn, total):
  def eval_fn(row):
    prediction = prediction_fn(row)
    expected = int((row['opening'] - row['p_opening']) > 0)
    total.add(1)
    if expected == 1 and prediction == expected:
      tp.add(1)
    elif expected == 1:
      fn.add(1)
    elif expected == 0 and prediction != expected:
      fp.add(1)
    else:
      tn.add(1)
  return eval_fn

def reset_acc(tp, fn, fp, tn, total):
  tp.value(0)
  fn.value(0)
  fp.value(0)
  tn.value(0)
  total.value(0)

def print_eval_result(prefix, tp, fn, fp, tn, total):
  print(f"{prefix} Accuracy: {(tp.value + tn.value) / total.value}")
  print(f"{prefix} Recall: {tp.value / (tp.value + fn.value)}")
  print(f"{prefix} Percision: {tp.value / (tp.value + fp.value)}")

def format_timestamp(ts):
  return datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:00:00')

if __name__ == "__main__":
  args = parse_args()
  eval_once = False

  conf = SparkConf().setAppName("Train Pricing Models")
  sc = SparkContext(conf=conf)
  # Need due to PipelineRDD has not attribute toDF
  sqlContext = SQLContext(sc)
  hiveContext = HiveContext(sc)

  t_df = get_tweet_dataframe(sc, "data-hourly-metrics")
  s_df = get_stock_dataframe(sc, "aapl_stock.csv")
  combine_df = t_df.join(s_df, "timestamp", "inner")

  tp = sc.accumulator(0)
  fn = sc.accumulator(0)
  fp = sc.accumulator(0)
  tn = sc.accumulator(0)
  total = sc.accumulator(0)

  if (args.sentiment_only) and not eval_once:
    eval_once = True
    prediction_fn = lambda x: int((x['sum_pos_sentiment'] - x['sum_neg_sentiment']) > 0)
    eval_fn = create_evaluation_fn(prediction_fn, tp, fn, fp, tn, total)
    combine_df.rdd.map(eval_fn).collect()
    print_eval_result("Sentiment-only", tp, fn, fp, tn, total)
  
  if (args.linear_regression or args.linear_regression_with_price) and not eval_once:
    eval_once = True
    log_prefix = "Linear-regression"
    model_folder = "model-lr"
    input_cols = [f"sum_{stat_name}" for stat_name in stats]
    if args.linear_regression_with_price:
      log_prefix += "-with-price"
      model_folder = "model-lrp"
      input_cols += ['p_opening']
    
    assembler = VectorAssembler(inputCols=input_cols, outputCol='features')
    vector = assembler.transform(combine_df)
    vector = vector.withColumn("label", combine_df["opening"])

    df = vector.withColumn("rank", percent_rank().over(Window.partitionBy().orderBy("timestamp")))
    train_df = df.where("rank <= .8").drop("rank")
    test_df = df.where("rank > .8").drop("rank")

    if os.path.exists(model_folder) and not args.force:
      lr_model = LinearRegressionModel.load(model_folder)
    else:
      lr = LinearRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)
      lr_model = lr.fit(train_df)
      lr_model.write().overwrite().save(model_folder)
    
    prediction = lr_model.transform(test_df)
    format_timestamp_udf = udf(lambda x: format_timestamp(x/1000))
    prediction = prediction.withColumn('timestamp', format_timestamp_udf(prediction['timestamp']))

    evaluator = RegressionEvaluator(labelCol="label", predictionCol="prediction", metricName="rmse")
    rmse = evaluator.evaluate(prediction)
    
    prediction_fn = lambda x: int((x['prediction'] - x['p_opening']) > 0)
    eval_fn = create_evaluation_fn(prediction_fn, tp, fn, fp, tn, total)
    prediction.rdd.map(eval_fn).collect()
    print_eval_result(log_prefix, tp, fn, fp, tn, total)
    print(f"{log_prefix} root mean squared error: {rmse}")
