from pyspark import SparkContext
from pyspark.sql import SparkSession
import os
import shutil
import random

# TODO: change input_dir to after sentiment score is run
input_dir = "data-sentiment"
output_dir = "data-hourly-metrics"

def parse_tuple(line):
  arr = line.split(',')
  timestamp = int(arr[0])
  sentiment = int(float(arr[1]))
  quote_cnt = int(arr[2])
  reply_cnt = int(arr[3])
  retweet_cnt = int(arr[4])
  favorite_cnt = int(arr[5])
  return (timestamp, int(sentiment==1), int(sentiment==0), quote_cnt, reply_cnt, retweet_cnt, favorite_cnt, 1)

def key_by_hour(tuple):
  ms_per_hour = 3600000
  hour = int(tuple[0] / ms_per_hour)
  return (hour * ms_per_hour, tuple[1:])

def sum_matrics(x, y):
  sum_pos_sentiment = x[0] + y[0]
  sum_neg_sentiment = x[1] + y[1]
  sum_quote = x[2] + y[2]
  sum_reply = x[3] + y[3]
  sum_retweet = x[4] + y[4]
  sum_favorite = x[5] + y[5]
  volume = x[6] + y[6]
  return (sum_pos_sentiment, sum_neg_sentiment, sum_quote, sum_reply, sum_retweet, sum_favorite, volume)

def flatten(tuple):
  return (tuple[0], *tuple[1])

if __name__ == "__main__":
  sc = SparkContext(appName='Aggregate Metrics By Hour')

  # Remove previous output
  if os.path.isdir(output_dir):
    shutil.rmtree(output_dir)
  
  in_path = os.path.join(input_dir, "part-*")
  rdd = sc.textFile(in_path)    \
    .map(parse_tuple)           \
    .map(key_by_hour)           \
    .reduceByKey(sum_matrics)   \
    .sortByKey()                \
    .coalesce(1)                \
    .map(flatten)               \
    .saveAsTextFile(output_dir)
