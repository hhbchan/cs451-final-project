#!/usr/bin/env python3

import yfinance as yf
import pandas as pd
import argparse

def parse_arg():
  parser = argparse.ArgumentParser(description="Download stock data")
  parser.add_argument("-output", type=str, help="output file path", default="appl_stock.csv")
  parser.add_argument("-start", type=str, default="2020-10-01", help="start date for stock data, must be formatted YYYY-MM-DD", )
  parser.add_argument("-end", type=str, default="2020-11-01", help="end date for stock data, must be formatted YYYY-MM-DD")
  parser.add_argument("-interval", type=str, default="1h", choices=["1m","2m","5m","15m","30m","60m","90m","1h","1d","5d","1wk","1mo","3mo"], help="granularity of time")
  parser.add_argument("-tickers", type=str, default="AAPL", help="stock market tickers")
  return parser.parse_args()


def main(output, start, end, interval, tickers):
  # download dataframe
  df_list = list()
  for ticker in tickers.split(","):
      data = yf.download(ticker, group_by="Ticker", start=start, end=end, interval=interval)
      data['ticker'] = ticker  # add this column becasue the dataframe doesn't contain a column with the ticker
      df_list.append(data)
  df = pd.concat(df_list)

  # Use this bottom line to output all the columns
  # df.to_csv(path_or_buf=output)
  df.to_csv(path_or_buf=output, columns=["Open", "Close", "ticker"], header=None)

if __name__ == "__main__":
  args = parse_arg()

  output = args.output
  start = args.start
  end = args.end
  interval = args.interval
  tickers = args.tickers

  main(output, start, end, interval, tickers)
