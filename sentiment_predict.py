import argparse
import os

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from pyspark.ml.tuning import CrossValidatorModel

from sentiment_train import preprocess, tfidf_transform

def parse_arg():
  parser = argparse.ArgumentParser(description="Tweet Predict")
  parser.add_argument("-input", type=str, help="dataset input path", required=True)
  parser.add_argument("-output", type=str, help="predict output path", required=True)
  return parser.parse_args()


def line_process(line):
  tokens = line.strip('()').split(',')
  timestamp = tokens[0]
  tweet = ",".join(tokens[1:len(tokens)-4])
  tweet = preprocess(tweet)

  return (timestamp, tweet, tokens[-4], tokens[-3], tokens[-2], tokens[-1])


if __name__ == "__main__":
  conf = SparkConf().setAppName("sentiment predict")
  sc = SparkContext(conf=conf)
  sqlContext = SQLContext(sc)

  args = parse_arg()

  input_dir = args.input
  output_dir = args.output

  in_path = os.path.join(input_dir, "*", "part-*")
  rdd = sc.textFile(in_path) \
    .map(lambda line: line_process(line)) 

  df = rdd.toDF(["timestamp", "tweet", "quote_cnt", "reply_cnt", "retweet_cnt", "favorite_cnt"])
  data = tfidf_transform(df)

  cv = CrossValidatorModel.load("cross_lr")
  predictions = cv.transform(data)

  predictions["timestamp", "prediction", "quote_cnt", "reply_cnt", "retweet_cnt", "favorite_cnt"] \
    .write.format("com.databricks.spark.csv").save(output_dir)
