from clint.textui import progress
import bz2
import os
import random
import requests
import shutil

# incomplete twitter data
exceptions = {(15, 15): range(0, 40), (15,16): [26, 59], (16, 6): range(0, 50), (16, 7): range(10, 60),
              (21, 20): [*range(0, 14)] + [25, 37, 38, 53, 54], (21, 21): range(9, 60), (31, 9): []}

downloaded_folder = "downloaded/"
data_folder = "data/"
os.makedirs(downloaded_folder, exist_ok=True)
os.makedirs(data_folder, exist_ok=True)

def main():
  failed_days = []
  for day in range(1, 32):
    date = f"2020-10-{day:02}"
    output_folder = data_folder + date + "/"

    print(f"Fetching for {date}")
    if os.path.exists(output_folder):
      print(f"  Data already exist for this day")
      continue
    os.makedirs(output_folder, exist_ok=True)

    try:
      fetch_day_twitter_data(day, output_folder)
    except:
      failed_days.append(day)
      remove_folder(output_folder)

  remove_folder(downloaded_folder)
  print_status(failed_days)  

def fetch_day_twitter_data(day, output_folder, items_per_hour = 2):
  baseurl = f"https://archive.org/download/archiveteam-twitter-stream-2020-10/"
  zip_name = f"twitter-stream-2020-10-{day:02}.zip/"
  for hour in progress.bar(range(24), expected_size=24):
    minute_range = range(29 if hour == 0 else 0, 60)
    if (day, hour) in exceptions:
      minute_range = exceptions[(day, hour)]
    if len(minute_range) >= items_per_hour:
      samples = random.sample(minute_range, items_per_hour)
    else:
      samples = [*minute_range]
    for minute in samples:
      bz2name = f"2020%2F10%2F{day:02}%2F{hour:02}%2F{minute:02}.json.bz2"
      filename = bz2name.replace('%2F', '-')
      url = baseurl + zip_name + bz2name
      download_path = downloaded_folder + filename
      output_path = output_folder + filename[:-4]
      download_twitter_bz2(url, download_path)
      decompress_bz2(download_path, output_path)

def download_twitter_bz2(url, output_path):
  r = requests.get(url, stream=True)
  with open(output_path, 'wb') as f:
      chunk_size = 4096
      for chunk in r.iter_content(chunk_size=chunk_size):
        f.write(chunk)

def decompress_bz2(bz2path, output_path):
  with open(output_path, 'wb') as nf, bz2.BZ2File(bz2path, 'rb') as cf:
    for data in iter(lambda : cf.read(100 * 1024), b''):
        nf.write(data)

def remove_folder(path):
  if os.path.isdir(path):
    shutil.rmtree(path) 

def print_status(failed_days):
  print("======================")
  if len(failed_days) == 0:
    print("Download Success")
  else:
    print("Download Failed for the following days")
    print(failed_days)
  print("======================")

if __name__ == "__main__":
  main()
