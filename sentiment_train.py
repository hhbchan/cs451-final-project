import re
import numpy as np
import pandas as pd

from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords

from pyspark import SparkContext, SparkConf
from pyspark.ml.feature import HashingTF, IDF
from pyspark.sql import SQLContext
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from pyspark.ml.classification import LinearSVC
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.classification import NaiveBayes
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator, CrossValidatorModel

# Defining dictionary containing all emojis with their meanings.
emojis = {':)': 'smile', ':-)': 'smile', ';d': 'wink', ':-E': 'vampire', ':(': 'sad', 
          ':-(': 'sad', ':-<': 'sad', ':P': 'raspberry', ':O': 'surprised',
          ':-@': 'shocked', ':@': 'shocked',':-$': 'confused', ':\\': 'annoyed', 
          ':#': 'mute', ':X': 'mute', ':^)': 'smile', ':-&': 'confused', '$_$': 'greedy',
          '@@': 'eyeroll', ':-!': 'confused', ':-D': 'smile', ':-0': 'yell', 'O.o': 'confused',
          '<(-_-)>': 'robot', 'd[-_-]b': 'dj', ":'-)": 'sadsmile', ';)': 'wink', 
          ';-)': 'wink', 'O:-)': 'angel','O*-)': 'angel','(:-D': 'gossip', '=^.^=': 'cat'}
stop_words = set(stopwords.words('english'))

def label_decoder(line):
  tokens = line.split(",")
  sentiment = int(str(tokens[0])[1])
  sentiment = 1 if (sentiment==4) else sentiment
  text = tokens[5]

  return (text, sentiment)

def preprocess(tweet):
  # Create Lemmatizer and Stemmer.
  wordLemm = WordNetLemmatizer()

  # Defining regex patterns.
  urlPattern        = r"((http://)[^ ]*|(https://)[^ ]*|( www\.)[^ ]*)"
  userPattern       = '@[^\s]+'
  alphaPattern      = "[^a-zA-Z0-9]"
  sequencePattern   = r"(.)\1\1+"
  seqReplacePattern = r"\1\1"

  tweet = tweet.lower()
        
  # Replace all URls with 'URL'
  tweet = re.sub(urlPattern,' URL',tweet)
  # Replace all emojis.
  for emoji in emojis.keys():
      tweet = tweet.replace(emoji, "EMOJI" + emojis[emoji])        
  # Replace @USERNAME to 'USER'.
  tweet = re.sub(userPattern,' USER', tweet)        
  # Replace all non alphabets.
  tweet = re.sub(alphaPattern, " ", tweet)
  # Replace 3 or more consecutive letters by 2 letter.
  tweet = re.sub(sequencePattern, seqReplacePattern, tweet)

  tweetwords = []

  for word in tweet.split():
      # Checking if the word is a stopword.
      if word not in stop_words and len(word)>1:
        # Lemmatizing the word.
        word = wordLemm.lemmatize(word)
        tweetwords.append(word)
  
  return tweetwords

def tfidf_transform(df):
  hashing_tf = HashingTF(inputCol="tweet", outputCol="rawFeatures")
  featurized_data = hashing_tf.transform(df)

  featurized_data.cache()
  idf = IDF(inputCol="rawFeatures", outputCol="features")
  idf_model = idf.fit(featurized_data)
  rescaled_data = idf_model.transform(featurized_data)

  return rescaled_data

def model_eval(algo, mpath):
  model = algo.fit(trainDF)
  model.save(mpath)

  predictions = model.transform(testDF)
  evaluator = BinaryClassificationEvaluator()
  eval_summary = evaluator.evaluate(predictions)
  return eval_summary

if __name__ == "__main__":
  conf = SparkConf().setAppName("sentiment train")
  sc = SparkContext(conf=conf)
  sqlContext = SQLContext(sc)

  prefixPath = "trained_model"

  rdd = sc.textFile("training.1600000.processed.noemoticon.csv") \
    .map(lambda line: label_decoder(line)) \
    .map(lambda p: (preprocess(p[0]), p[1]))

  df = rdd.toDF(["tweet", "label"])
  
  rescaledData = tfidf_transform(df)
  
  rescaledData.select("label", "features").show(20, truncate=30)

  seed = 0  # set seed for reproducibility
  trainDF, testDF = rescaledData.randomSplit([0.8,0.2],seed)

  # logistic regression
  lr = LogisticRegression(maxIter=20, regParam=0.3, elasticNetParam=0)
  
  # linear support vector machine
  lsvc = LinearSVC(maxIter=10, regParam=0.1)
  
  # naive bayes
  nb = NaiveBayes(smoothing=1.0, modelType="multinomial")

  # cross validation - lr
  paramGrid_lr = ParamGridBuilder() \
    .addGrid(lr.regParam, np.linspace(0.3, 0.01, 5)) \
    .addGrid(lr.elasticNetParam, np.linspace(0.0, 0.8, 5)) \
    .build()
  crossval_lr = CrossValidator(estimator=lr,
                            estimatorParamMaps=paramGrid_lr,
                            evaluator=BinaryClassificationEvaluator(),
                            numFolds= 3)  

  print("lr eval. summary: ", model_eval(lr, "lr"))
