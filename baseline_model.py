from pyspark import SQLContext,SparkConf,SparkContext
from pyspark.sql.window import Window
from random import randrange

from pyspark.sql import SQLContext
from pyspark.sql import HiveContext
from pyspark.sql.functions import *

import argparse
import time
import os
import shutil

def parse_arg():
  parser = argparse.ArgumentParser(description="Create baseline model to determine up/down")
  parser.add_argument("--input", type=str, help="input file path", default="data-hourly-metrics/aggregated.txt")
  parser.add_argument("--output", type=str, help="output file path", default="baseline_model_output")
  return parser.parse_args()

def parse_line(line):
  arr = line.strip('()').split(',')
  return (arr[0], arr[1], arr[2])

def get_rdd(sc, input_file):
  return sc.textFile(input_file) \
    .map(parse_line)  \

def compute_result(row):
  if row["sum_zeros"] > row["sum_ones"]:
    return (int(row["timestamp"]), "down")
  elif row["sum_zeros"] == row["sum_ones"]:
    return (int(row["timestamp"]), "same")
  return (int(row["timestamp"]), "up")

def main():
  args = parse_arg()

  input_file = args.input
  output_dir = args.output

  # Remove previous output
  if os.path.isdir(output_dir):
    shutil.rmtree(output_dir)

  # Set-up Spark configs
  conf = SparkConf().setAppName("baseline train")
  sc = SparkContext(conf=conf)
  sqlContext = SQLContext(sc)
  hiveContext = HiveContext(sc)

  # Create dataframe so we can manipulate the table
  rdd = get_rdd(sc, input_file)
  df = rdd.toDF(["timestamp", "zeros", "ones"])

  # Initialize the window
  sliding_window_size = 3

  # Note the -1, sliding window is the row itself AND the next sliding_window_size rows
  window_spec = Window.rowsBetween(Window.currentRow, sliding_window_size-1).orderBy(desc("timestamp"))
  all_spec = Window.rowsBetween(Window.unboundedPreceding, Window.currentRow).orderBy(desc("timestamp"))

  # Remove rows that will be out of the window
  num_rows = df.count()
  max_row = num_rows - (num_rows % (sliding_window_size+1))

  # Actually apply the sliding window and name those columns
  df = df.withColumn("sum_zeros", sum("zeros").over(window_spec))
  df = df.withColumn("sum_ones", sum("ones").over(window_spec))
  df = df.withColumn("row_number",row_number().over(all_spec))
  df = df.filter("row_number <= %d" %max_row)
  
  # Print the output to a directory
  df.rdd.map(compute_result).saveAsTextFile(output_dir)

if __name__ == "__main__":
  main()
