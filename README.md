# CS451 final project
In this project, we tackle the old but exciting problem of predicting the stock market. In traditional technical analysis, we utilize various metrics such as the relative strength index (RSI) to quantify market conditions. These techniques rely on computing some metrics based on the historical stock market data. In this project, we are going to augment these techniques with the insight we obtain from analyzing various big data sources, such as social media networks or news outlets, to improve their predicting power. We will  use big data frameworks such as Apache Spark or Apache Hadoop to predict how the market will change in the near future (from CS451 assignment desccription).

Video Presentation: [https://www.youtube.com/watch?v=1xBZX2lZp7U](https://www.youtube.com/watch?v=1xBZX2lZp7U)

## Implmentation Overview
![flow chart](./flow-chart.png)

## Get Started
This project uses `Python3` (version 3.6+) and `pyspark`. Please make sure that `Apache Spark` is properly installed already. For other python packages used in the following scripts, you can install packages using `pip3` by doing:
```
pip3 install -r requirement.txt
```

## Preparing Data
Next, we need to download twitter data from an archieve and decompmress it using the following command. This script currently downloads unfiltered twitter data for the month of October 2020. For the sake of reducing data size and downloading time, the script samples data by picking 2 random minute of each hour. Currently it take ≈3.5min to download a day of data and ≈2hrs for the script to finish. Enjoy your cup of coffee in the meanwhile ☕☕

Note that the random 2 minute is not uniform because for all 00 hour, data collection start at 29 minute mark. There are also missing data noted by the `exceptions` global variable in the script. If the scripts fails, please check the archive to see that if the particulate date that it fails on is missing data. You can see which day you are stuck on from stdout and the hour by the progeress bar. Once identified, added a new exception mapping `(day, hour): range/list of day`, re-run the script. If success, commit your changes.

source from https://archive.org/details/archiveteam-twitter-stream-2020-10
```
python3 download_data.py
```

The downloaded twitter data is unfiltered meaning that it will contains all tweets posted on Twitter. In this project, we are only interested in tweet that are financial-related. The following spark script do some light filtering.
```
export PYSPARK_PYTHON=/usr/bin/python3
spark-submit --master local[4] filter_finance_tweets.py 
```

Next, we also need to download stock data that is used as label to train our model
```
python3 stockdata.py -interval 1h -start 2020-10-01 -end 2021-11-01 -output aapl_stock.csv
```
Note it can accept optional arguments such as input, output, start, end, interval, tickers. Check the code for the defaults. This parameters in above command are picked to work with training scripts.

## Sentiment Analysis
To train sentiment analysis model on `Sentiment140` dataset, run the following command. Since the model uses cross validation and take approximate an hour to run, a trained model is stored in this repo and you may skip this step and move on to applying prediction.
```
spark-submit --master local[4] sentiment_train.py
```
To predict sentiment value on tweet data, run:
```
spark-sumbit --master local[4] sentiment_predict.py --input data-finance-filtered --output data-sentiment
```

## Pricing Model
To train and eval a model run the following command:
```
spark-submit --master local[4] pricing_model.py (-so|-lr|-lrp) [-f]
```
- `-so` train comparison model with sentiment-only (ie positive vs negative over 3 hours period)
- `-lr` train linear regression model with sentiment and tweet metrics
- `-lrp` train linear regression model with sentiment, tweet metrics, and previous stock price

The script will also output basic evaluation metric such as accuracy, recall, percision, and root square mean error for linear regresssion model. To obtain those value, do `grep (Sentiment-only|Linear-regression|Linear-regression-with-price)` repsectively on the terminal output. 

## Result
|                                    | Accuracy | Recall | Precision | Other       |
|------------------------------------|----------|--------|-----------|-------------|
| Baseline (sentiment only)          | 45.695%  | 100%   | 45.695%   |             |
| Linear Regression(without pricing) | 76.67%   | 100%   | 66.667%   | RMSE: 3.104 |
| Linear Regression(with pricing)    | 73.33%   | 100%   | 63.336%   | RMSE: 2.720 |
